<?php
namespace KDA\Filament\MediaManager;
use KDA\Laravel\PackageServiceProvider;

//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasViews;
    use \KDA\Laravel\Traits\HasProviders;
    /* This is mandatory to avoid problem with the package path */
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
     // trait \KDA\Laravel\Traits\HasConfig; 
     //    registers config file as 
     //      [file_relative_to_config_dir => namespace]
    protected $configDir='config';
    protected $configs = [
         'kda/filament-media-manager.php'  => 'kda.filament-media-manager'
    ];
        //register filament provider
    protected $additionnalProviders=[
        \KDA\Filament\MediaManager\FilamentServiceProvider::class
    ];

    protected  $viewNamespace ="kda-media-manager";
    protected $publishViewsTo= 'vendor/kda-media-manager';
    /*public function register()
    {
        parent::register();
    }*/
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){

        
    }
}
