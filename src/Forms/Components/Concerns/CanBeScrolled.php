<?php

namespace KDA\Filament\MediaManager\Forms\Components\Concerns;
use Closure;


trait CanBeScrolled{
    protected bool | Closure  $scrollable = false;
    
    public function scrollable(bool | Closure  $scrollable): static
    {
        $this->scrollable = $scrollable;
        return $this;
    }
    public function getScrollable(): bool
    {
        return $this->evaluate($this->scrollable);
    }
   

}
