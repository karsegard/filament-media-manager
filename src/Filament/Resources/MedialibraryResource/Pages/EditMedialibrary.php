<?php

namespace KDA\Filament\MediaManager\Filament\Resources\MedialibraryResource\Pages;

use KDA\Filament\MediaManager\Filament\Resources\MedialibraryResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditMedialibrary extends EditRecord
{
    protected static string $resource = MedialibraryResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
