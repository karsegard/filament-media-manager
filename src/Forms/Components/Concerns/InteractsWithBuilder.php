<?php

namespace KDA\Filament\MediaManager\Forms\Components\Concerns;


trait InteractsWithBuilder{
    protected bool | Closure | null $interactsWithBuilder = false;

    
    public function builder(bool | Closure $builder): static
    {
        $this->interactsWithBuilder = $builder;

        return $this;
    }


    public function interactsWithBuilder(): bool
    {
        return $this->evaluate($this->interactsWithBuilder) ?? false;
    }
}
