<?php

namespace KDA\Filament\MediaManager\Forms\Components\Concerns;
use Closure;


trait HasFlavor{
    protected string | Closure | null $flavor = 'medialibrary_flavor';
    
    public function flavor(string | Closure $flavor): static
    {
        $this->flavor = $flavor;

        return $this;
    }


    public function getFlavor(): string
    {
        return $this->evaluate($this->flavor) ?? config('kda.medialibrary-item.default_flavor');
    }
}
