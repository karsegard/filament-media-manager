<?php

namespace KDA\Filament\MediaManager\Forms\Components\Concerns;
use Closure;


trait HasConversion{
    protected string $conversion = 'thumb-md';

    public function displayConversion(string | Closure  $conversion): static
    {
        $this->conversion = $conversion;
        return $this;
    }
    public function getDisplayConversion(): string
    {
        return $this->evaluate($this->conversion);
    }

   
}
