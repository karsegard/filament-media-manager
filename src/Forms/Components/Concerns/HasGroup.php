<?php

namespace KDA\Filament\MediaManager\Forms\Components\Concerns;
use Closure;


trait HasGroup{
    protected string | Closure  $group = '';
    
    public function group(string | Closure  $group): static
    {
        $this->group = $group;
        return $this;
    }

    public function getGroup()
    {
        $group =  $this->evaluate($this->group) ?? '';
        $state = $this->getState();
        if($this->interactsWithBuilder() && !blank($state)){
            $group = $group."_".($state['uuid']?? (string)\Str::uuid());
        }
        return $group;
    }
}
