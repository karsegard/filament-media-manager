<?php

namespace KDA\Filament\MediaManager\Livewire;

use KDA\Eloquent\MedialibraryItem\Models\MediaLibraryItem;
use Livewire\Component;
use Livewire\WithFileUploads;

class Uploads extends Component
{
    use WithFileUploads;

    public $photos = [];
    public $modalId;
    public $field;

    public function clear(){
        $this->photos=[];
    }
    public function render()
    {
        return view('kda-media-manager::livewire.mediamanager.uploads');
    }

    public function save()
    {
        $this->validate([
            'photos.*' => 'image|max:20024', // 1MB Max
        ]);
        foreach ($this->photos as $photo) {
            MedialibraryItem::add($photo)->store();
        }
        $this->clear();
        $this->emit('uploaded-files');

        $this->dispatchBrowserEvent('close-modal', ['id' => $this->modalId,  'field' => $this->field]);
    }
}
