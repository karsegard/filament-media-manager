<x-forms::field-wrapper :id="$getId()" :label="$getLabel()" :label-sr-only="$isLabelHidden()" :helper-text="$getHelperText()" :hint="$getHint()"
    :hint-icon="$getHintIcon()" :required="$isRequired()" :state-path="$getStatePath()">
    @php
        $items = $getState()['items'] ?? [];
    @endphp
    <div x-data="{
        state: $wire.entangle('{{ $getStatePath() }}'),
        loading: false,
        preview:false,
        previewImage: '',
        addMedia: (media_id) => {
            $wire.dispatchFormEvent('multiplemediaitempicker::add-media', '{{ $getStatePath() }}', media_id);
        },
        openModal: () => {
            $dispatch('open-modal', { id: 'multiple-medialibrary.media-item-modal', fieldId: '{{ $getStatePath() }}', isMultiple: true })
        },
        showPreview(image){
            this.previewImage = image;
            this.preview = true;
        },
        closePreview(){
            this.preview=false;
        },
        init :()=>{
            console.log('init');
            let el =$refs.sortable;
            let options = { draggable: '.draggable',mirror:{constrainDimensions:true} }

            if (el.querySelector('.handle')) {
                options.handle ='.handle'
            }
            const sortable = new Draggable.Sortable(el,options);
            sortable.on('sortable:stop', () => {
                setTimeout(() => {
                    let items = {}

                    el.querySelectorAll('.draggable').forEach((el, index) => {
                        let value = el.getAttribute('sortable-value');
                        items[value]= index+1
                    })
                    console.log('sent',items);
                    $wire.dispatchFormEvent('multiplemediaitempicker::reordered-media', '{{ $getStatePath() }}',items);
                }, 1)
            })
        }
    }"
        x-on:close-modal.window="$event.detail.fieldId == '{{ $getStatePath() }}' ? addMedia($event.detail.media) : null">
        <div @class([
            'bg-white rounded-md relative w-full min-h-[8rem] blueprint-media',
            'max-h-[20rem] overflow-y-scroll' => $getScrollable(),
        ])>
        <div class="absolute z-20  w-full h-full  bg-black" x-on:click="closePreview" x-cloak x-show="preview">
            <img :src="previewImage"/>
        </div>
            <div x-show="this.loading"
                class="absolute z20 bg-black w-full h-full opacity-75 flex justify-center content-center items-center">
                <div class="opacity-100">Loading</div>
            </div>
                <div class="grid grid-cols-4 gap-1 " x-ref="sortable">

                    @if ($getState())
                    @php
                        $state = $getState();
                        $items = $state['items']??[];
                    @endphp
                    @foreach ($items as $item)
                            <div class="group relative draggable flex justify-center items-center bg-black"  wire:key="{{ $getStatePath() }}-task-{{ $item['medialibrary_item_id'] }}" sortable-value="{{$item['medialibrary_item_id']}}">
                                <div @class([
                                    'group-hover:visible invisible bg-white absolute top-1 left-px p-2 flex flex-col gap-2',
                                ])>
                                <button type="button" class="handle">
                                    <x-heroicon-o-arrows-expand class="w-5 h-5"/>
                                </button>
                                <button type="button" x-on:click="showPreview('{{ $item['preview'] }}')">
                                    <x-heroicon-o-search class="w-5 h-5"/>
                                </button>
                            </div>
                                <div @class([
                                    'group-hover:visible invisible bg-white absolute top-1 right-1 p-2 flex flex-col',
                                ])>
                                    <button type="button" x-on:click="$wire.dispatchFormEvent('multiplemediaitempicker::detach-media', '{{ $getStatePath() }}',{{$item['medialibrary_item_id']}});">
                                        <x-heroicon-o-trash class="w-5 h-5" />
                                    </button>
                                </div>
                                <img src="{{ $item['preview'] }}" class="aspect-square object-cover" />
                            </div>
                        @endforeach
                        {{--@foreach ($getItems() as $item)
                            <div class="group relative draggable flex justify-center items-center bg-black"  wire:key="{{ $getStatePath() }}-task-{{ $item->id }}" sortable-value="{{$item->id}}">
                                <div @class([
                                    'group-hover:visible invisible bg-white absolute top-1 left-px p-2 flex flex-col gap-2',
                                ])>
                                <button type="button" class="handle">
                                    <x-heroicon-o-arrows-expand class="w-5 h-5"/>
                                </button>
                                <button type="button" x-on:click="showPreview('{{ $item->default_media->getUrl('md') }}')">
                                    <x-heroicon-o-search class="w-5 h-5"/>
                                </button>
                            </div>
                                <div @class([
                                    'group-hover:visible invisible bg-white absolute top-1 right-1 p-2 flex flex-col',
                                ])>
                                    <button type="button" x-on:click="$wire.dispatchFormEvent('multiplemediaitempicker::detach-media', '{{ $getStatePath() }}',{{$item->id}});">
                                        <x-heroicon-o-trash class="w-5 h-5" />
                                    </button>
                                </div>
                                <img src="{{ $item->default_media->getUrl('thumb-sm') }}" class="aspect-square object-cover" />
                            </div>
                        @endforeach--}}
                    @else
                        no images selected
                    @endif
                </div>
           
        </div>
        <x-filament::button type="button" x-on:click="openModal()">
            Add Media
        </x-filament::button>
        @if(count($items)>0)
            <x-filament::button type="button" color="danger"
                    :wire:click="'dispatchFormEvent(\'multiplemediaitempicker::detach-all-media\', \'' . $getStatePath() . '\')'">
                    remove all
            </x-filament::button>
        @endif
    </div>
        @pushonce('scripts')
        <script src="https://cdn.jsdelivr.net/npm/@shopify/draggable@1.0.0-beta.8/lib/draggable.bundle.js"></script>

        
        @endpushonce
    @once

        @push('modals')
            <livewire:filament-media-manager-modal modalId="multiple-medialibrary.media-item-modal" uploadModalId="multiple-medialibrary.uploads"/>
            <livewire:filament-media-manager-upload-modal modalId="multiple-medialibrary.uploads" />
        @endpush
    @endonce
</x-forms::field-wrapper>
