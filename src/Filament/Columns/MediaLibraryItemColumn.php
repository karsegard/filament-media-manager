<?php
namespace KDA\Filament\MediaManager\Filament\Columns;

use Filament\Tables\Columns\ImageColumn;




class MediaLibraryItemColumn extends ImageColumn
{
    protected string $view = 'kda-media-manager::tables.columns.image-column';

    protected function setUp(): void
    {
        parent::setUp();

        /*$this->extraImgAttributes(function($record){
            $focal = $record->meta['focal_point'] ?? '50% 50%';
            return [
                'class'=>'object-cover w-full absolute inset-0 rounded-t-xl',
                'style'=>'object-position:'.$focal.';'
            ];
        });*/

        $this->extraAttributes([
            'style' => app()->getLocale() == 'ar' ? 'margin:-12px -16px 0px -40px' : 'margin:-12px -40px 0px -16px',
        ])
        ->extraImgAttributes(
            function($record){
                $focal = $record->meta['focal_point'] ?? '50% 50%';
                return [
                    'class' => 'object-cover h-fit rounded-t-xl w-full',
                    'style'=>'object-position:'.$focal.';'
                ];
            })
        ;
    }
}
