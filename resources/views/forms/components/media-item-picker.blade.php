<x-forms::field-wrapper :id="$getId()" :label="$getLabel()" :label-sr-only="$isLabelHidden()" :helper-text="$getHelperText()" :hint="$getHint()"
    :hint-icon="$getHintIcon()" :required="$isRequired()" :state-path="$getStatePath()">
    <div x-data="{ 
        state: $wire.entangle('{{ $getStatePath() }}'),
       
        openModal: ()=>{
            console.log(this.state);
            $dispatch('open-modal', {id: 'medialibrary.media-item-modal', fieldId:'{{ $getStatePath() }}',selected:this.state})
        }
    
    }"
        x-on:close-modal.window="$event.detail.fieldId == '{{ $getStatePath() }}'  && $wire.dispatchFormEvent('mediaitempicker::attach','{{$getStatePath()}}',$event.detail.media)">
        {{--@dump($getState())--}}
            <div x-on:click="openModal()"
                class="cursor-pointer relative bg-white rounded-md  w-full min-h-[16rem] blueprint-media flex justify-center content-center items-center">
                @if (!$isEmpty())
                    @php
                       $state = $getState();
                       $focus = $state['meta']['focal_point'] ?? null;
                       $style="";
                       if(!blank($focus)){
                            $style = "object-position:".$focus.";";
                       }
                    @endphp
                    <img src="{{ $state['preview'] ?? ''}}" style="{{$style}}" class="rounded-md absolute inset-0 h-full w-full object-cover" />
                @else
                    no image selected, click to select
                @endif
                <div class="absolute top-1 right-1 bg-white rounded-full">
                <button title="{{ __('forms::components.builder.buttons.delete_item.label') }}" type="button"
                x-on:click.stop="$wire.dispatchFormEvent('mediaitempicker::detach', @js($getStatePath()))"
                    @class([
                        'flex items-center justify-center flex-none w-10 h-10 text-danger-600 transition hover:text-danger-500',
                      
                        'dark:text-danger-500 dark:hover:text-danger-400' => config(
                            'forms.dark_mode'
                        ),
                        'hidden'=>$isEmpty()
                    ])>
                    <span class="sr-only">
                        {{ __('forms::components.builder.buttons.delete_item.label') }}
                    </span>

                    <x-heroicon-s-trash class="w-4 h-4" />
                </button>
            </div>
            </div>
           {{-- <x-filament::button type="button"
                x-on:click="openModal()">
                {{ $getButtonLabel() }}
            </x-filament::button>
            @if (!$isRequired() && $getState())
                <x-filament::button type="button" color="danger"
                    :wire:click="'dispatchFormEvent(\'mediaitempicker::detach\', \'' . $getStatePath() . '\')'">
                    detach
                </x-filament::button>
            @endif
            --}}
       
    </div>
    @once
        @push('modals')
        <livewire:filament-media-manager-modal modalId="medialibrary.media-item-modal" uploadModalId="medialibrary.uploads"/>
        <livewire:filament-media-manager-upload-modal modalId="medialibrary.uploads" />

        @endpush
    @endonce
</x-forms::field-wrapper>
