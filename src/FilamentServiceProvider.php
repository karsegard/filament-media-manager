<?php

namespace KDA\Filament\MediaManager;
use Filament\PluginServiceProvider;
use KDA\Filament\MediaManager\Filament\Resources\MedialibraryResource;
use Livewire\Livewire;
use Spatie\LaravelPackageTools\Package;

class FilamentServiceProvider extends PluginServiceProvider
{
    protected array $styles = [
    //    'my-package-styles' => __DIR__ . '/../dist/app.css',
        'filament-media-manager' => __DIR__ . '/../resources/dist/css/media-manager.css',
    ];

    protected array $scripts = [
       'filament-media-manager' => 'https://cdn.jsdelivr.net/npm/@shopify/draggable@1.0.0-beta.8/lib/draggable.bundle.js',
    ];


    protected array $widgets = [
    //    CustomWidget::class,
    ];

    protected array $pages = [
    //    CustomPage::class,
    ];

    protected array $resources = [
        MedialibraryResource::class
   //     CustomResource::class,
    ];
 
    public function configurePackage(Package $package): void
    {
        $package->name('filament-media-manager');
        
    }

    public function packageBooted(): void
    {
        parent::packageBooted();
        Livewire::component('filament-media-manager-modal',\KDA\Filament\MediaManager\Livewire\MediaItemModal::class);
        Livewire::component('filament-media-manager-upload-modal',\KDA\Filament\MediaManager\Livewire\Uploads::class);
    }
}
