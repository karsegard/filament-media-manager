<?php

namespace KDA\Filament\MediaManager\Forms\Components;

use Closure;
use Johncarter\FilamentFocalPointPicker\Fields\FocalPointPicker as FieldsFocalPointPicker;

class FocalPointPicker extends FieldsFocalPointPicker{


    public function imageField(string $field): static
    {

        return $this->image(function (Closure $get) use ($field) {
            $imageState = collect($get($field))?->first();
            return $imageState;
        });
    }
    
}