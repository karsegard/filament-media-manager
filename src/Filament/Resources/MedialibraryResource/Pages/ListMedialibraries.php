<?php

namespace KDA\Filament\MediaManager\Filament\Resources\MedialibraryResource\Pages;

use KDA\Filament\MediaManager\Filament\Resources\MedialibraryResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListMedialibraries extends ListRecords
{
    protected static string $resource = MedialibraryResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
    protected function shouldPersistTableFiltersInSession(): bool
    {
        return true;
    }
}
