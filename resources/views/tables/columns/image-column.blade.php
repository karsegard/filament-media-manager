<div {{ $attributes->merge($getExtraAttributes())->class([
    'filament-tables-image-column',
    'px-4 py-3' => ! $isInline(),
]) }}>
    @php
        $height = $getHeight();
        $width = $getWidth() ?? ($isCircular() || $isSquare() ? $height : null);
        $style = "";

        $style.= $height !== null ? "height: {$height};" : '';
        $style.= $width !== null ? "width: {$width};" : '';
    @endphp

    <div
        style="
            {!! $height !== null ? "height: {$height};" : null !!}
            {!! $width !== null ? "width: {$width};" : null !!}
        "
        @class([
            'overflow-hidden' => $isCircular() || $isSquare(),
            'rounded-full' => $isCircular(),
        ])
    >
   
        @if ($path = $getImagePath())
            <img
                src="{{ $path }}"
             
                {{ $getExtraImgAttributeBag()->class([
                    'object-cover object-center' => $isCircular() || $isSquare(),
                ])->merge([
                    'style'=> $getExtraImgAttributeBag()->prepends($style)
                ],false) }}
            >
       @endif
    </div>
</div>
