<?php

namespace KDA\Filament\MediaManager\Filament\Resources;

use KDA\Filament\MediaManager\Filament\Resources\MedialibraryResource\Pages;
use KDA\Filament\MediaManager\Filament\Resources\MedialibraryResource\RelationManagers;
use Filament\Forms;
use Filament\Forms\Components\ViewField;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Actions\Action;
use Filament\Tables\Actions\ActionGroup;
use Filament\Tables\Columns\BadgeColumn;
use Filament\Tables\Columns\ImageColumn;
use Filament\Tables\Columns\Layout\Grid;
use Filament\Tables\Columns\Layout\Panel;
use Filament\Tables\Columns\Layout\Split;
use Filament\Tables\Columns\Layout\Stack;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Columns\ViewColumn;
use Filament\Tables\Filters\Filter;
use Filament\Tables\Filters\TernaryFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Johncarter\FilamentFocalPointPicker\Fields\FocalPointPicker;
use KDA\Eloquent\MedialibraryItem\Models\MediaLibraryItem;
use KDA\Filament\MediaManager\Filament\Columns\MediaLibraryItemColumn;

class MedialibraryResource extends Resource
{
    protected static ?string $model = MediaLibraryItem::class;

    protected static ?string $navigationIcon = 'heroicon-o-film';

    protected static function getNavigationGroup(): ?string
    {
        return __('filament.navigation.groups.media');
    }

    public static function form(Form $form): Form
    {
        return $form->schema([
            //
            ViewField::make('preview_image')->view('kda-media-manager::forms.components.image'),

            FocalPointPicker::make('meta.focal_point')
                ->default('10% 25%') // default: "50% 50%"
                ->image(fn($record) => $record->preview_image),
        ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                MediaLibraryItemColumn::make('preview_image')->height(200),

                Grid::make()
                    ->schema([
                        BadgeColumn::make('curators_count')
                            ->counts('curators')
                            ->color(function ($state) {
                                return match ($state == 0) {
                                    true => 'danger',
                                    false => 'success',
                                };
                            })
                            ->formatStateUsing(function ($state) {
                                return $state > 0 ? 'used ' . $state . ' times' : 'not used';
                            })
                            ->extraAttributes([
                                //  'class'=>'absolute left-0 top-0 bg-white'
                            ]),

                        BadgeColumn::make('meta.focal_point')
                            ->color(function ($state) {
                                return match (blank($state)) {
                                    true => 'danger',
                                    false => 'success',
                                };
                            })
                            ->formatStateUsing(function ($state) {
                                return !blank($state) ? 'focal point set' : 'focal point not set';
                            })
                            ->extraAttributes([
                                'class' => '-mr-6 rtl:-ml-6 rtl:mr-0',
                            ])
                            ->alignEnd(),
                    ])
                    ->columns(2),
                Grid::make()
                    ->schema([
                        TextColumn::make('file_name')
                            ->extraAttributes([
                                'class' => 'text-ellipsis overflow-hidden ... truncate text-gray-500 dark:text-gray-300 text-xs',
                            ])
                            ->alignJustify()
                            ->wrap(true)
                            ->sortable(),
                    ])
                    ->columns(1),

                TextColumn::make('created_at')
                    ->date()
                    ->extraAttributes([
                        'class' => 'text-gray-500 dark:text-gray-300 text-xs',
                    ])
                    ->prefix('created ')
                    ->sortable(),
            ])
            ->filters([
                TernaryFilter::make('with_focal_point')->nullable()->attribute('meta->focal_point'),
                
            ])
            ->defaultSort('created_at', 'desc')
            ->actions([
                ActionGroup::make([
                    Tables\Actions\EditAction::make(),
                 /*   Action::make('focal')
                    ->mountUsing(fn ( $form,  $record) => $form->fill([
                        'preview_image' => $record->preview_image
                    ]))
                        ->form(function($record){
                            return [

                                FocalPointPicker::make('meta.focal_point')
                                    ->default('10% 25%') // default: "50% 50%"
                                    ->image(fn($record) => $record->preview_image),
                                ];
                        })
                        ->action(function () {}),*/
                ]),
            ])
            ->bulkActions([Tables\Actions\DeleteBulkAction::make()])
            ->contentGrid([
                'md' => 2,
                'xl' => 4,
            ]);
    }

    public static function getRelations(): array
    {
        return [
                //
            ];
    }

   
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListMedialibraries::route('/'),
            'create' => Pages\CreateMedialibrary::route('/create'),
            'edit' => Pages\EditMedialibrary::route('/{record}/edit'),
        ];
    }
}
