<?php

namespace KDA\Filament\MediaManager\Livewire;

use KDA\Eloquent\MedialibraryItem\Models\MediaLibraryItem;
use Livewire\Component;
use Livewire\WithPagination;

class MediaItemModal extends Component
{
    use WithPagination;

    public $isMultiple = false;
    public $selected = null;
    public $selecteds = [];
    public $modalId ;
    public $uploadModalId;
    public $fieldId = null;
    protected $listeners = ['uploaded-files' => '$refresh'];

    public function clearSelected(){
        $this->selecteds=[];
        $this->selected =null;
        $this->setPage(1);
    }
    public function save()
    {
        if ($this->isMultiple) {
            $this->dispatchBrowserEvent('close-modal', ['id' => $this->modalId, 'media' => $this->selecteds, 'fieldId' => $this->fieldId]);
        } else {
            $this->dispatchBrowserEvent('close-modal', ['id' => $this->modalId, 'media' => $this->selected, 'fieldId' => $this->fieldId]);
        }
    }

    public function render()
    {
        $items = MediaLibraryItem::paginate(20);
        return view('kda-media-manager::livewire.mediamanager.media-item-modal', ['media' => $items]);
    }

    public function pick($media)
    {
        if ($this->isMultiple) {
            if (in_array($media, $this->selecteds)) {
                $idx = array_search($media, $this->selecteds);
                unset($this->selecteds[$idx]);
            } else {
                $this->selecteds[] = $media;
            }
        }else{
            $this->selected= $media;
        }
    }
}
/*
<?php

namespace App\Forms\Components;

use Filament\Forms\Components\Field;
use Livewire\Component;
use Filament\Facades\Filament;
use Filament\Forms\Contracts\HasForms;
use Filament\Forms;
use Filament\Forms\Concerns\InteractsWithForms;

class MediaItemModal extends Component implements HasForms
{
    use InteractsWithForms;

    public function render()
    {
        return view('forms.components.media-item-modal');
    }
}

*/
