<?php

namespace KDA\Filament\MediaManager\Forms\Components\Concerns;
use Closure;


trait HasButtonLabel{
    protected string | Htmlable | Closure | null $buttonLabel = null;
    protected bool $shouldTranslateButtonLabel = false;
    public function buttonLabel(string | Htmlable | Closure | null $buttonLabel): static
    {
        $this->buttonLabel = $buttonLabel;

        return $this;
    }

    public function translateButtonLabel(bool $shouldTranslateLabel = true): static
    {
        $this->shouldTranslateButtonLabel = $shouldTranslateLabel;

        return $this;
    }
    public function getButtonLabel(): string | Htmlable | null
    {
        $label =  $this->evaluate($this->buttonLabel);
        return is_string($label) && $this->shouldTranslateButtonLabel
            ? __($label)
            : $label;
    }
}
