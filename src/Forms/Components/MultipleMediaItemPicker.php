<?php

namespace KDA\Filament\MediaManager\Forms\Components;

use Closure;
use Filament\Forms\Components\Field;
use KDA\Eloquent\MedialibraryItem\Models\MediaLibraryItem;
use Filament\Notifications\Notification;
use KDA\Eloquent\MedialibraryItem\Facades\MediaManager;

class MultipleMediaItemPicker extends Field
{
    use Concerns\HasButtonLabel;
    use Concerns\HasGroup;
    use Concerns\HasConversion;
    use Concerns\CanBeScrolled;
    use Concerns\HasFlavor;
    use Concerns\InteractsWithBuilder;


    protected string $view = 'kda-media-manager::forms.components.multiple-media-item-picker';

    //public $selecteds =  [];
    //public $loading = false;

    public function loadState()
    {
        /* $media = $this->getRecord()->getMediaItemsByGroup($this->getGroup())->orderBy("pivot_sort_value")->get();
        if ($media) {
            $this->state($media->toArray());
        }*/

        $medias = $this->getStoredMediaItems();
        $this->state($this->mediasToStateArray($medias));
    }

    public function mediaToStateArray(MediaLibraryItem $media): array
    {
        $result = [];
        $preview = MediaLibraryItem::find($media->id)?->default_media?->getUrl('thumb-md');

        $result['medialibrary_item_id'] = $media->id;
        $result['sort'] = $media?->curator?->sort_value ?? 0;

        $result['preview'] = $preview;
        return $result;
    }

    public function getStoredMediaItems()
    {

        return $this->getRecord()?->getMediaItemsByGroup($this->getGroup())?->orderBy("pivot_sort_value")?->get();;
    }

    public function mediasToStateArray($medias): array
    {
        $state = $this->getState();
        $results = ['items' => []];
        if ($this->interactsWithBuilder()) {
            $results['uuid'] = $state['uuid'] ?? (string)\Str::uuid();
            //  $result['group'] = $this->getGroup()."_".$result['uuid'];
        } //else{
        //    $result['group'] = $this->getGroup();
        //}

        foreach ($medias as $media) {
            if ($media) {
                $result = $this->mediaToStateArray($media);
                $results['items'][] = $result;
            }
        }
        return $results;
    }


    protected function setUp(): void
    {
        $this->defaultState = collect([]);
        parent::setUp();
        $this->buttonLabel = __('Add media item');


        $this->loadStateFromRelationshipsUsing(static function (MultipleMediaItemPicker $component, $record): void {
            $component->loadState();
        });
        //$this->dehydrated(false); // ??not sure why it's set to false
        $this->saveRelationshipsUsing(static function (MultipleMediaItemPicker $component, $state) {

            $component->updateMedias();

            return $state;
        });

        $this->afterStateHydrated(static function (MultipleMediaItemPicker $component, $state): void {
            
            if ($component->getRecord()) {
                if ($component->interactsWithBuilder() && !isset($state['uuid'])) {
                    $state['uuid'] = (string)\Str::uuid();
                    //  $result['group'] = $this->getGroup()."_".$result['uuid'];
                } 
                 $component->state($state);
            } else {
                $component->state([]);
            }
        });

        /*   $this->dehydrateStateUsing(static function (MultipleMediaItemPicker $component, $state) {
        });
*/


        $this->registerListeners([
            'multiplemediaitempicker::detach-all-media' => [
                function (MultipleMediaItemPicker $component, string $statePath) {
                    if ($component->isDisabled()) {
                        return;
                    }
                    if ($statePath !== $component->getStatePath()) {
                        return;
                    }
                    $state = $this->getState();
                    $newstate = [];
                    $state['items'] = $newstate;
                    $component->state($state);
                    /*   if ($component->getRecord()) {
                        $component->getRecord()->mediaLibraryItems()->wherePivot('group', $this->getGroup())->detach($media);
                    }
                    $component->loadState();*/
                }
            ],
            'multiplemediaitempicker::detach-media' => [
                function (MultipleMediaItemPicker $component, string $statePath, $media) {
                    if ($component->isDisabled()) {
                        return;
                    }
                    if ($statePath !== $component->getStatePath()) {
                        return;
                    }
                    $state = $this->getState();
                    $newstate = array_filter($state['items'], function ($item) use ($media) {
                        return $media != $item['medialibrary_item_id'];
                    });
                    $state['items'] = $newstate;
                    $component->state($state);
                    /*   if ($component->getRecord()) {
                        $component->getRecord()->mediaLibraryItems()->wherePivot('group', $this->getGroup())->detach($media);
                    }
                    $component->loadState();*/
                }
            ],
            'multiplemediaitempicker::reordered-media' => [
                function (MultipleMediaItemPicker $component, string $statePath, $items) {
                    if ($component->isDisabled()) {
                        return;
                    }
                    if ($statePath !== $component->getStatePath()) {
                        return;
                    }

                    $state = $this->getState();
                    $new_items = [];
                    $existing_items = collect($state['items'] ?? [])->groupBy('medialibrary_item_id');
                    $values = array_flip(collect($items)->sort()->toArray());
                    
                    foreach ($values as $count=>$key) {
                        $item = $existing_items->get($key)->first();
                        $item['sort']=$count;
                        $new_items[] =$item;
                        
                    }

                    $state['items'] = $new_items;

                    $component->state($state);
                    /*if (!$component->getRecord()) {
                        $state =  collect($this->getState())->groupBy('id');
                        $newstate = [];

                        $values = array_flip(collect($items)->sort()->toArray());
                        // dd($values);
                        foreach ($values as $key) {
                            $newstate[] = $state->get($key)->first();
                        }
                        //    dump($items,$values,$state,$newstate);
                        $component->state($newstate);
                        return;
                    }
                    $component->getRecord()->updateMediaItemsOrders($component->getRecord()->getMediaItemsByGroup($component->getGroup()), $items);
                    $component->loadState();*/
                }
            ],
            /*      'multiplemediaitempicker::start-loading' => [
                function (MultipleMediaItemPicker $component, string $statePath) {
                    if ($component->isDisabled()) {
                        return;
                    }
                    if ($statePath !== $component->getStatePath()) {
                        return;
                    }
                    $this->loading = true;
                }
            ],
*/
            'multiplemediaitempicker::add-media' => [
                function (MultipleMediaItemPicker $component, string $statePath, $media_ids): void {
                    $added = 0;
                    $skipped = 0;
                    if ($component->isDisabled()) {
                        return;
                    }
                    if ($statePath !== $component->getStatePath()) {
                        return;
                    }
                    $state = $component->getState() ?? [];

                    $medias = MediaLibraryItem::whereIn('id', $media_ids)->get();
                    foreach ($medias as $m) {
                        $items = $state['items'] ?? [];
                        $existing = array_filter($items, function ($item) use ($m) {
                            return $m->id == $item['medialibrary_item_id'];
                        });
                        
                        if (count($existing) == 0) {
                            $item = $this->mediaToStateArray($m);
                            $item['sort']=count($items);
                            $state['items'][] = $item;
                            //$this->attachMedia($m);
                            //$state[]
                            $added++;
                        } else {
                            $skipped++;
                        }
                    }

                    $component->state($state);
                    /* foreach ($medias as $m) {
                        $existing = array_filter($state, function ($item) use ($m) {
                            return $m->id == $item['id'];
                        });

                        if (count($existing) == 0) {
                            $state[] = $m;
                            $this->attachMedia($m);
                            $added++;
                        } else {
                            $skipped++;
                        }
                    }
                    if ($skipped > 0) {
                        Notification::make()
                            ->title($skipped . ' Media skipped because already present')
                            ->success()
                            ->send();
                    }
                    if ($added > 0) {
                        Notification::make()
                            ->title($added . ' Media added successfuly')
                            ->success()
                            ->send();
                    }
                    $component->state($state);*/
                    //$this->loading = false;
                },
            ],
            /* 'mediaitempicker::detach' => [
                function (MultipleMediaItemPicker $component, string $statePath): void {
                    if ($component->isDisabled()) {
                        return;
                    }
                    if ($statePath !== $component->getStatePath()) {
                        return;
                    }
                 //   dd('hello', $statePath, $component->getDisplayConversion(), $component->getGroup(), $component->getRecord());
                    $component->getRecord()->getMediaItemsByGroup($component->getGroup())->detach();
                    $component->state(null);
                },
            ],*/

        ]);
    }


    public function attachMedia($media)
    {
        if ($this->getRecord()) {
            if ($media) {
                if (is_array($media)) {
                    $media = $media['id'];
                }
                if (!($media instanceof MediaLibraryItem)) {
                    $media = MediaLibraryItem::find($media);
                }

                $this->getRecord()->addMedia($media)->setMultiple(true)->inGroup($this->getGroup())->usingFlavor($this->getFlavor());
            }
        }
    }
    public function updateOrder($items)
    {

        $model = $this->getModel();
        $model::updateMediaItemsOrders($this->getRecord()->getMediaItemsByGroup($this->getGroup()), $items);
    }

    public function updateMedias()
    {
        if ($this->getRecord()) {
            $state = $this->getState();
           /* $this->getRecord()->clearMedia($this->getFlavor(), $this->getGroup());
            if ($state) {
                $items = $state['items'];
                if (count($items) > 0) {
                    $order = collect($items)->pluck('sort', 'medialibrary_item_id')->toArray();
                    
                    foreach ($items as $media) {
                        if (is_array($media)) {
                            $media = $media['medialibrary_item_id'];
                        }

                        if (!($media instanceof MediaLibraryItem)) {
                            $media = MediaLibraryItem::find($media);
                        }
                        $group = $this->getGroup();
                        $this->getRecord()->addMedia($media)->setMultiple(true)->inGroup($group)->usingFlavor($this->getFlavor());
                    }
                    $this->updateOrder($order);
                }
            }*/
            if(!isset($state['items'])){
                $state['items']=[];
            }
            $order = collect($state['items'])->pluck('sort', 'medialibrary_item_id')->toArray();

            $items = collect($state['items'])->map(function($item){
                if (is_array($item)) {
                    $item = $item['medialibrary_item_id'];
                }

                if (!($item instanceof MediaLibraryItem)) {
                    $item = MediaLibraryItem::find($item);
                }
                return $item;
            })->pluck('id')->toArray();

            MediaManager::updateMediaItems($items,$this->getRecord(),$this->getFlavor(),$this->getGroup());

            $this->updateOrder($order);

        }
    }




    public function getItems()
    {
        $state = $this->getState();
        return array_map(function ($item) {
            return $this->getItem($item);
        }, $state);
    }
    public function getItem($item)
    {
        $is_medialibrary = ($item instanceof MediaLibraryItem);
        if (!$is_medialibrary) {
            $item = MediaLibraryItem::find($item['id']);
        }
        /*if($this->getRecord()){
            if(!$is_medialibrary){
                $item = MediaLibraryItem::find($item['id']);
            }
           // return $item->getFirstMediaByFlavor($this->getFlavor())?->getUrl($this->getDisplayConversion());
            return $item->default_media->getUrl('thumb-sm');
        }else{
            if(!$is_medialibrary){
                $item = MediaLibraryItem::find($item['id']);
            }
            return $item->default_media->getUrl('thumb-sm');

        }*/
        return $item;
    }
}
