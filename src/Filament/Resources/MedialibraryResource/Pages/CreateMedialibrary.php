<?php

namespace KDA\Filament\MediaManager\Filament\Resources\MedialibraryResource\Pages;

use KDA\Filament\MediaManager\Filament\Resources\MedialibraryResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateMedialibrary extends CreateRecord
{
    protected static string $resource = MedialibraryResource::class;
}
