<div x-data="{
    isOpen: false,
    field: @entangle('field'),

    openModal(event) {
        console.log(event.detail.id)
        if (event.detail.id === '{{ $modalId }}') {
        console.log('hey');

            this.isOpen = true;
            this.field = event.detail.field;
            $wire.clear();
        }
    },
}" role="dialog" aria-modal="true"
    x-on:close-modal.window="if ($event.detail.id === '{{ $modalId }}') isOpen = false;"
    x-on:open-modal.window="openModal($event)">
    <div x-show="isOpen" x-transition:enter="ease duration-300" x-transition:enter-start="opacity-0"
        x-transition:enter-end="opacity-100" x-transition:leave="ease duration-300" x-transition:leave-start="opacity-100"
        x-transition:leave-end="opacity-0" x-cloak
        class="fixed inset-0 z-50 flex items-center min-h-screen p-10 overflow-y-auto transition">
        <button x-on:click="isOpen = false; " type="button" aria-hidden="true"
            class="fixed inset-0 w-full h-full bg-black/50 focus:outline-none filament-curator-media-picker-modal-close-overlay"></button>
            <div  class="max-h-full z-20 h-3/4 mx-auto p-2 space-y-2 bg-white rounded-xl cursor-default pointer-events-auto filament-modal-window w-4/5 max-w-6xl">
        <form wire:submit.prevent="save" class="" x-data="drop_file_component()">

            <input type="file" accept="image/*" wire:model="photos">
            @error('photos.*')
                <span class="error">{{ $message }}</span>
            @enderror
            <div class="mt-1" wire:loading wire.target="photos"
            x-on:livewire-upload-progress="progress = $event.detail.progress">
                <svg class="animate-spin -ml-1 mr-3 h-5 w-5 text-gray-700" xmlns="http://www.w3.org/2000/svg"
                    fill="none" viewBox="0 0 24 24">
                    <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor"
                        stroke-width="4"></circle>
                    <path class="opacity-75" fill="currentColor"
                        d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z">
                    </path>
                </svg>
                <div>Processing Files</div>
               
            </div>
            <div class="grid grid-cols-12"
                x-bind:class="dropingFile ? 'bg-gray-400 border-gray-500' : 'border-gray-500 bg-gray-200'"
                x-on:drop="dropingFile = false" x-on:drop.prevent="
    handleFileDrop($event)
    "
                x-on:livewire-upload-start="isUploading = true" x-on:livewire-upload-finish="isUploading = false"
                x-on:livewire-upload-error="isUploading = false;hasError=true; alert('error')"
                
                x-on:dragover.prevent="dropingFile = true" x-on:dragleave.prevent="dropingFile = false">
                <div class="text-center" wire:loading.remove wire.target="photos">Drop Your Files Here</div>
                
              

                @foreach ($photos as $key => $photo)
                    <div wire:key="{{ $key }}" class="rounded-sm bg-slate-50 p-4"> <img class="rounded-xl"
                            class="w-full" src="{{ $photo->temporaryUrl() }}"></div>
                @endforeach
            </div>
            <x-filament::button wire:loading.defer.500ms.attr="disabled" type="submit">Save Photo</x-filament::button>
            <script>
                function drop_file_component() {
                    return {
                        isUploading: false,
                        progress: 0,
                        hasError: false,
                        dropingFile: false,
                        processing:false,
                        handleFileDrop(e) {
                            console.log('test');
                            if (event.dataTransfer.files.length > 0) {
                                processing=true;
                                const files = e.dataTransfer.files;
                                for (let file of files) {
                                    @this.upload('photos', file,
                                        (...args) => {console.log(args)}, (...e) => console.error(e), (event) => {}
                                    )
                                }
                                processing=false;

                            }
                        }
                    };
                }
            </script>
        </form>
    </div>
    </div>
</div>
