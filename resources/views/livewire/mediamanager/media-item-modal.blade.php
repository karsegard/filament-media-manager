<div x-data="{
    isOpen: false,
    selected: @entangle('selected'),
    selecteds: @entangle('selecteds'),
    fieldId: @entangle('fieldId'),
    isMultiple: @entangle('isMultiple'),
    openModal(event) {
        if (event.detail.id === '{{ $modalId }}') {
            this.isOpen = true;
            this.fieldId = event.detail.fieldId;
            this.isMultiple = event.detail.isMultiple ? true : false;
            $wire.clearSelected();
        }
    },
}" role="dialog" aria-modal="true"
    x-on:close-modal.window="if ($event.detail.id === '{{ $modalId }}') isOpen = false;"
    x-on:open-modal.window="openModal($event)">
    <div x-show="isOpen" x-transition:enter="ease duration-300" x-transition:enter-start="opacity-0"
        x-transition:enter-end="opacity-100" x-transition:leave="ease duration-300" x-transition:leave-start="opacity-100"
        x-transition:leave-end="opacity-0" x-cloak
        class="fixed inset-0 z-40 flex items-center min-h-screen p-10 overflow-y-auto transition">
        <button x-on:click="isOpen = false; " type="button" aria-hidden="true"
            class="fixed inset-0 w-full h-full bg-black/50 focus:outline-none filament-curator-media-picker-modal-close-overlay"></button>

        <div
            class="max-h-full z-20 h-4/5- mx-auto p-2 space-y-2 bg-white rounded-xl cursor-default pointer-events-auto filament-modal-window w-4/5 max-w-6xl">
            <div class="px-4 py-2 filament-modal-header">
                <h2 class="text-xl font-bold tracking-tight filament-modal-heading">
                    media
                </h2>

                <div class="grid grid-cols-8 w-full h-full gap-3">
                    @foreach ($media as $key => $m)
                        @if ($isMultiple)
                            <div wire:key="{{ $key }}" @class([
                                'rounded-sm bg-slate-50 p-4',
                                'border-2 border-lime-400' => in_array($m->id, $selecteds),
                            ])>
                                <img class="rounded-xl" x-on:click.prevent="$wire.pick( {{ $m->id }})"
                                    class="w-full" src="{{ $m->default_media?->getUrl('thumb-sm') }}" />
                            </div>
                        @else
                            <div wire:key="{{ $key }}" @class([
                                'rounded-sm bg-slate-50 p-4',
                                'border-2 border-lime-400' => $m->id == $selected,
                            ])>
                                <img class="rounded-xl" x-on:click.prevent="$wire.pick( {{ $m->id }})"
                                    class="w-full" src="{{ $m->default_media?->getUrl('thumb-sm') }}" />
                            </div>
                        @endif
                    @endforeach

                </div>
                <div class="w-full min-w-full">
                    {{ $media->links() }}
                </div>
                <x-filament::button type="button" wire:click="save">
                    select
                </x-filament::button>
                <x-filament::button type="button"
                    x-on:click=" $dispatch('open-modal', { id: '{{ $uploadModalId }}', field: this.fieldId, isMultiple: true })">
                    upload images
                </x-filament::button>
            </div>
        </div>
    </div>
</div>
