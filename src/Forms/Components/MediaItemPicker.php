<?php

namespace KDA\Filament\MediaManager\Forms\Components;

use Closure;
use Filament\Forms\Components\Field;
use Illuminate\Support\Collection;
use KDA\Eloquent\MedialibraryItem\Models\MediaLibraryItem;

class MediaItemPicker extends Field
{
    use Concerns\HasFlavor;
    use Concerns\HasGroup;
    use Concerns\HasButtonLabel;
    use Concerns\InteractsWithBuilder;
    use Concerns\HasConversion;

    protected string $view = 'kda-media-manager::forms.components.media-item-picker';

    public function saveRelation($state){
        $this->attachMedia();
       // return $this->getState();
    }

    public function loadState($state){
        if ($this->getRecord()) {
            /*if($this->interactsWithBuilder()){
                $group = $state;
                $state = $this->getMediaItemsByGroup($group)?->first()?->toArray() ;
            }*/
            // $media = $this->getRecord()->mediaLibraryItems->first();
            $media = $this->getStoredMediaItem();

           // if ($media) {
                $this->state($this->mediaToStateArray($media));
            //}
        }
    }

    public function isEmpty(){

        $state= $this->getState();

       // $media_id = $state['medialibrary_item_id'] ?? null;
        return blank($state) || blank($state['preview']);
    }

    public function transformState($state){
       
        if(isset($state['preview'])){
            unset($state['preview']);
        }
        if(isset($state['meta'])){
            unset($state['meta']);
        }
        return $state;
    }

    public function getStoredMediaItem(){
        $group = $this->getGroup();
        $state = $this->getState();
       /* if($this->interactsWithBuilder() && !blank($state)){
            $group = $state['group'];
        }*/
        $media = $this->getRecord()?->getMediaItemsByGroup($group)
            ?->first();
        return $media;
    }

    public function getMediaItemsByGroup($group):Collection{
        return $this->getRecord()?->getMediaItemsByGroup($group)->get();
    }

    public function getMediaItem($id):MediaLibraryItem{
        return MediaLibraryItem::find($id);
    }


    public function mediaToStateArray($media):array{
        $state = $this->getState();
        $result = [];
        if( $this->interactsWithBuilder()){
            $result['uuid']= $state['uuid'] ?? (string)\Str::uuid();
          //  $result['group'] = $this->getGroup()."_".$result['uuid'];
        }//else{
        //    $result['group'] = $this->getGroup();
        //}
        $result['preview'] = '';
        if($media){
            $preview = $media?->default_media?->getUrl('thumb-md');
        
            $result ['medialibrary_item_id']=$media->id;
            $result ['meta']= $media->meta;
            $result['preview']=$preview;
        }
        return $result;
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->buttonLabel('Pick Media') ->translateButtonLabel(true);



        $this->saveRelationshipsUsing(static function (MediaItemPicker $component, $state) {
            $component->saveRelation($state);
        });

        $this->afterStateHydrated(static function (MediaItemPicker $component, $state): void {
            $component->loadState($state);
        });

        $this->dehydrateStateUsing(static function (MediaItemPicker $component, $state) {
           return $component->transformState($state);
        });

        $this->registerListeners([
            'mediaitempicker::detach' => [
                function (MediaItemPicker $component, string $statePath): void {
                    if ($component->isDisabled()) {
                        return;
                    }
                    if ($statePath !== $component->getStatePath()) {
                        return;
                    }
                    //   dd('hello', $statePath, $component->getDisplayConversion(), $component->getGroup(), $component->getRecord());
                    $component->getRecord()->getMediaItemsByGroup($component->getGroup())->detach();
                    $component->state(null);
                },
            ],

            'mediaitempicker::attach' => [
                function (MediaItemPicker $component, string $statePath,$media_id): void {
                    if ($component->isDisabled()) {
                        return;
                    }
                    
                    if ($statePath !== $component->getStatePath()) {
                        return;
                    }
                    $media = $component->getMediaItem($media_id);
                    $component->state($component->mediaToStateArray($media));
                },
            ],

        ]);
    }

    public function attachMedia()
    {
        if ($this->getRecord()) {
           
            $state = $this->getState();
            if(!isset($state['medialibrary_item_id'])){
                return ;
            }
           /* if($this->interactsWithBuilder() && \Str::isUuid($media)){
                return ;
            }
            if ($media) {
                $group = $this->getGroup();
                if($this->interactsWithBuilder() ){
                    $group = \Str::uuid();
                    $this->state($group); 
                }
                if (is_array($media)) {
                    $media = $media['id'];
                }
                if (!($media instanceof MediaLibraryItem)) {
                    $media = MediaLibraryItem::find($media);
                }

                $this->getRecord()->addMedia($media)->inGroup($group)->usingFlavor($this->getFlavor());
            }*/

            $group = $this->getGroup();
           /* if($this->interactsWithBuilder()){
                $group = $state['group'];
            }*/
            $flavor = $this->getFlavor();
        
            $media = $this->getMediaItem($state['medialibrary_item_id']);
            $this->getRecord()->addMedia($media)->inGroup($group)->usingFlavor($flavor);
        }
    }

  

   /* public function getCurrentItem()
    {
        $record = $this->getRecord();
        $state = $this->getState();
        if (blank($state)) {
            return null;
        }
        //$name = $this->getName();
        $is_medialibrary = ($state instanceof MediaLibraryItem);
        if ($record && $is_medialibrary) {
            $media = $record->getMediaItemsByGroup($this->getGroup())
                ->first()
                ?->getFirstMediaByFlavor($this->getFlavor());

            $path = $media->getUrl($this->getDisplayConversion());
            if (isset($media->generated_conversions[$this->getDisplayConversion()])) {
                if (!empty($path)) {
                    return $path;
                }
            }
        }
        if ($record && $this->interactsWithBuilder() && \Str::isUuid($state)) {
            $mediaItem = $record->getMediaItemsByGroup($state)
                ->first();
            
            $media = $mediaItem?->getFirstMediaByFlavor($this->getFlavor());

            $path = $media?->getUrl($this->getDisplayConversion());
            if (isset($media->generated_conversions[$this->getDisplayConversion()])) {
                if (!empty($path)) {
                    return $path;
                }
            }else{
                return $mediaItem->default_media->getUrl('thumb-md');
            }
        }
      

        if (!$record || blank($state) || !($state instanceof MediaLibraryItem)) {
            $id = $this->getState();
            if (is_array($this->getState())) {
                $id = $this->getState()['id'];
            }
            return MediaLibraryItem::find($id)->default_media->getUrl('thumb-md');
        } elseif ($this->getState() instanceof MediaLibraryItem) {
            return $this->getState()->default_media->getUrl('thumb-md');
        }
    }*/
}
