<?php

namespace KDA\Filament\MediaManager\Forms\Components\Concerns;
use Closure;


trait IsMultiple{
    protected bool | Closure  $multiple = false;

    public function isMultiple(bool | Closure $multiple): static
    {
        $this->multiple = $multiple;

        return $this;
    }

    public function getIsMultiple(): bool
    {
        return $this->evaluate($this->multiple);
    }
   
}
